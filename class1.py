from art import tprint


class Hello:
    def __init__(self, str):
        self.str = str

class Hi(Hello):
    def ttprint(self):
        tprint(self.str)
